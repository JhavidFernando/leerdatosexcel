/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author madar
 */
public class SopaDeLetras {
    
    private char sopa[][];

    public SopaDeLetras() {
    }
    
    
    public void leerMatrizExcel(String nombreArchivo) throws FileNotFoundException, IOException
    {
    HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream("src/Datos/sopa.xls"));
    HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
        System.out.println("Filas:"+canFilas);
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol=filas.getLastCellNum();
            
        for(int j=0;j<cantCol;j++)    
        {
            //Obtiene la celda y su valor respectivo
            //double r=filas.getCell(i).getNumericCellValue();
            String valor=filas.getCell(j).getStringCellValue();
            System.out.print(valor+"\t");
        }
     System.out.println();
       }
        
    
    
    }
    
    
}
